#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>
using namespace std;

void copyFile(string file1, string file2) {

  ifstream fs;
  ofstream ft;
  char ch;
  string str;
  fs.open (file1);
  ft.open (file2);
  if(fs&&ft){
    while(getline(fs,str)) {
      ft << str << endl;
    }
  }

  cout << "File has been copied successfully!" << endl;
  fs.close();
  ft.close();
}

int main (int argc, char * argv[]) {
  std::ofstream myfile;
  myfile.open ("input.txt");
  myfile << "Hi, I'm Faiq Baig." << "\n" << "Per Aspera Ad Astra, this sentence is in Latin which means Through the hardships to the stars." << endl;
  myfile.close();

  if (argc != 3) {

    cout << "This program needs 2 arguments!" << endl;

    return 0;
  }

  copyFile (argv[1], argv[2]);
  return 0;
}
