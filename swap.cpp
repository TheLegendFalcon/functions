#include <iostream>
using namespace std;

void array_print(int arr[], int size){
    cout << "[";
    for(int i = 0; i < size; i++){
        if(i == (size-1)){
            cout << arr[i];
        }else{
            cout << arr[i] << ", ";
        }
    }
    cout << "]";
}

void swap(int arr[], int pos1, int pos2){
    int val1, val2;
    val1 = arr[pos1];
    val2 = arr[pos2];
    arr[pos2] = val1;
    arr[pos1] = val2;
}

int main() {
    int num[] = {1, 2, 3, 4, 5, 6, 7, 8};
    int num_size = sizeof(num)/sizeof(num[0]);
    swap(num, 0, 7);
    array_print(num, num_size);
}
