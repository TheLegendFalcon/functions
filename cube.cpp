#include <iostream>
using namespace std;

double cube(double  n);

int main(){
  int n;
  double c;

  cout << "Enter any number: ";
  cin >> n;

  c = cube(n);

  cout << "Cube of " << n << " is " << c << endl;

  return 0;
}

double cube(double num)
{
  return (num * num * num);
}
