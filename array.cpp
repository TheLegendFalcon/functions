#include <iostream>
using namespace std;

void array_print(int arr[], int size) {
  cout << "[";

  for(int i  = 0; i < size; i++) {
    if (i == (size-1)) {
    cout << arr[i];
  } else {
    cout << arr [i] << ", ";
  }
}
cout << "]";
}

int main() {

  int number[] = {1, 2, 3};
  int n_size = sizeof(number)/sizeof(number[0]);

  array_print(number, n_size);
}
